#pragma once

#include <QtWidgets/QWidget>
#include "ui_DemoWidget.h"

class DemoWidget : public QWidget
{
    Q_OBJECT

public:
    DemoWidget(QWidget *parent = nullptr);
    ~DemoWidget();

private:
    Ui::DemoWidgetClass ui;
};
