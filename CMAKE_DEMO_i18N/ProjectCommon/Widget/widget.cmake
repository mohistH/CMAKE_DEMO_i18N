set(widget_root 
${CMAKE_CURRENT_LIST_DIR}/src
)

file( GLOB_RECURSE
widget_files 
${widget_root}/*.h
${widget_root}/*.cpp
)

set(widget_inc
${widget_root}
)