
# 拷贝文件， 参数1-文件， 参数2-目标目录
macro(copy_file_macro src_file dst_root)

    foreach(which_file ${src_file})
        execute_process( COMMAND ${CMAKE_COMMAND} -E copy ${which_file} ${dst_root})
    endforeach(which_file ${src_fi})
        
endmacro(copy_file_macro src_file dst_root)



# 创建文件夹
macro(create_folder_macro target_folder)
    
    # 创建 public_include 指向的文件夹
    execute_process( COMMAND ${CMAKE_COMMAND} -E make_directory ${target_folder})

endmacro(create_folder_macro target_folder)


# 拷贝文件夹
macro(copy_folder_macro src_folder target_folder)
    execute_process( COMMAND ${CMAKE_COMMAND} -E copy_directory ${src_folder} ${target_folder})
endmacro(copy_folder_macro src_folder target_folder)



set(output_prefix 
	${common_output_root}/publish
)

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
# using Clang
	set(compiler_name clang)
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
# using GCC
	set(compiler_name gcc)
	
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
	set(compiler_name intel)
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
	if(MSVC_VERSION EQUAL 1400)
		set(compiler_name vc80)
	elseif(MSVC_VERSION EQUAL 1500)
		set(compiler_name vc90)
	elseif(MSVC_VERSION EQUAL 1600)
		set(compiler_name vc100)
	elseif(MSVC_VERSION EQUAL 1700)
		set(compiler_name vc110)
	elseif(MSVC_VERSION EQUAL 1800)
		set(compiler_name vc120)
	elseif(MSVC_VERSION EQUAL 1900)
		set(compiler_name vc140)
	elseif(MSVC_VERSION EQUAL 1910)
		set(compiler_name vc141)
	elseif(MSVC_VERSION EQUAL 1920)
		set(compiler_name vc142)
	elseif(MSVC_VERSION EQUAL 1930)
		set(compiler_name vc143)
	else()
		set(compiler_name vcXX)
	endif()
	
	# message("MSC_VER=${MSC_VER}")
	
endif()



# 64位
if (CMAKE_CL_64)
	set (platform "x64")
# 32
else()
	set (platform "x86")
endif()


# publish/vc100/x64
set(publish_prefix ${output_prefix}/${compiler_name}/${platform})

# 指定include目录
set(pir ${publish_prefix}/include)


# 指定可执行程序输出目录
set(pbd 	${publish_prefix}/bin/Debug)
set(pbr 	${publish_prefix}/bin/Release)
# 指定 库文件输出目录
set(pld 	${publish_prefix}/lib/Debug)
set(plr 	${publish_prefix}/lib/Release)

# 设置 可执行程序输出目录
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${pbd})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${pbr})

# 设置库文件输出目录
# set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${pld})
# set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${plr})

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${pld})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${plr})


# cmake文件夹
set(
	common_cmake_root 
	${publish_prefix}/cmake
)

create_folder_macro(${pir})

create_folder_macro(${pld})
create_folder_macro(${plr})
create_folder_macro(${pbd})
create_folder_macro(${pbr})
create_folder_macro(${common_cmake_root})

# 拷贝 dll到输出目录
macro(copy_dll_to_publish_root WHICH_TARGET )
	if (EXISTS ${pbd})
		# 获取DEBUG属性
		get_property(debug_bin_file TARGET ${WHICH_TARGET} PROPERTY IMPORTED_LOCATION_DEBUG)
		# 拷贝DLL到输出目录
		copy_file_macro(${debug_bin_file} ${pbd})
	endif()

	if (EXISTS ${pbr})
		# 获取 RELEASE 属性
		get_property(release_bin_file TARGET ${WHICH_TARGET} PROPERTY IMPORTED_LOCATION_RELEASE)
		# 拷贝DLL到输出目录
		copy_file_macro(${release_bin_file} ${pbr})
	endif()
endmacro( )





# i18N
function(CREATE_QM_UPDATE_TS)
	set(option)
	# TARGET_NAME-项目名称
	set(oneValueArgs TS_ROOT TARGET_NAME)
	# ALL_TS_FILE-所有文件
	# ALL_TRANSLATE_ROOT-要翻译哪些文件夹下的源码文件
	set(multiValueArgs ALL_TS_FILE ALL_TRANSLATE_ROOT)
	# 固定写法
	cmake_parse_arguments(TS_QM "${option}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

	set(update_all_ts_files ${WHICH_PROJECT_NAME}_UPDATE_ALL_TS)
    set(create_all_qm_files ${WHICH_PROJECT_NAME}_CREATE_QM_FILE)

	add_custom_target(${update_all_ts_files} ALL)
	add_custom_target(${create_all_qm_files} ALL)

	# 手动查找翻译需要的程序
	IF (CMAKE_SYSTEM_NAME MATCHES "Linux")
		find_file(LUPDATE_PATH lupdate ${qt_bin_dir})
		find_file(LRELEASE_PATH lrelease ${qt_bin_dir})
	ELSEIF (CMAKE_SYSTEM_NAME MATCHES "Windows")
		find_file(LUPDATE_PATH lupdate.exe ${qt_bin_dir})
		find_file(LRELEASE_PATH lrelease.exe ${qt_bin_dir})
	endif()

	# 把Ts文件提取出来
	foreach(TS_FILE ${TS_QM_ALL_TS_FILE})
		# 得到文件名
		get_filename_component(LOCAL_LANGUAGE_NAME ${TS_FILE} NAME_WE)
		# 指定TS项目名称
		set(TS_TARGET_NAME "${WHICH_PROJECT_NAME}_UPATE_TS_${LOCAL_LANGUAGE_NAME}")
		# 用于更新项目文件中的翻译
		add_custom_target(${TS_TARGET_NAME}
			COMMAND ${LUPDATE_PATH} ${TS_QM_ALL_TRANSLATE_ROOT} -ts ${TS_FILE}	
			VERBATIM
			)

			# 将 update_ts_file_<name>添加到update_all_ts_file项目的依赖
			add_dependencies(${update_all_ts_files} ${TS_TARGET_NAME})
			# 准备qm文件
			set(QM_TARGET_NAME "${WHICH_PROJECT_NAME}_CREATE_QM_${LOCAL_LANGUAGE_NAME}")
			set(QM_FILE "${TS_QM_TS_ROOT}/${LOCAL_LANGUAGE_NAME}.qm")

			# qm文件转换
			add_custom_target(${QM_TARGET_NAME} 
			COMMAND ${LRELEASE_PATH} ${TS_FILE} -qm ${QM_FILE} 
			VERBATIM
			)

			# 因为须先有TS文件，才能生成qm文件
			add_dependencies(${QM_TARGET_NAME} ${TS_TARGET_NAME})
			add_dependencies(${create_all_qm_files} ${QM_TARGET_NAME})

			SET_PROPERTY(TARGET ${create_all_qm_files} PROPERTY FOLDER "LANGUAGE")
			SET_PROPERTY(TARGET ${QM_TARGET_NAME} PROPERTY FOLDER "LANGUAGE")
			SET_PROPERTY(TARGET ${update_all_ts_files} PROPERTY FOLDER "LANGUAGE")
			SET_PROPERTY(TARGET ${TS_TARGET_NAME} PROPERTY FOLDER "LANGUAGE") 
		
	endforeach(TS_FILE ${TS_QM_ALL_TS_FILE})
	
	IF (NOT ${WHICH_PROJECT_NAME})
		add_dependencies(${WHICH_PROJECT_NAME} ${QM_TARGET_NAME})
	ENDIF()

endfunction()

        

       

       

#         foreach(TS_FILE ${TS_FILES})
#                 # 把zh_CN.ts中的zh_CN提取出来
#                 get_filename_component(LOCAL_LANGUAGE_NAME ${TS_FILE} NAME_WE)
#                 set(TS_TARGET_NAME "${target_name}_update_ts_file_${LOCAL_LANGUAGE_NAME}")
                
#                 # 遍历目录 
#                 foreach( which_root ${update_src_dir})
# 					 add_custom_target(${TS_TARGET_NAME}    COMMAND ${LUPDATE_PATH} ${which_root} -ts ${TS_FILE} VERBATIM)
#                 endforeach()
                
               

#                 # 将update_ts_file_<NAME>添加为update_all_ts_files的依赖，下同
#                 add_dependencies(${update_all_ts_files} ${TS_TARGET_NAME})

#                 set(QM_TARGET_NAME "${target_name}_create_qm_file_${LOCAL_LANGUAGE_NAME}")
#                 set(QM_FILE "${bin_qrc_dir}/${LOCAL_LANGUAGE_NAME}.qm")

#                 add_custom_target(${QM_TARGET_NAME}  COMMAND ${LRELEASE_PATH} ${TS_FILE} -qm ${QM_FILE}    VERBATIM)

#                 # 因为得先有ts文件才能生成qm文件，所以把构建ts文件的目标作为自己的依赖

                
#                 add_dependencies(${QM_TARGET_NAME} ${TS_TARGET_NAME})
#                 add_dependencies(${create_all_qm_files} ${QM_TARGET_NAME})


#                 SET_PROPERTY(TARGET ${create_all_qm_files} PROPERTY FOLDER "LANGUAGE")
#                 SET_PROPERTY(TARGET ${QM_TARGET_NAME} PROPERTY FOLDER "LANGUAGE")
#                 SET_PROPERTY(TARGET ${update_all_ts_files} PROPERTY FOLDER "LANGUAGE")
#                 SET_PROPERTY(TARGET ${TS_TARGET_NAME} PROPERTY FOLDER "LANGUAGE") 
#         endforeach()
# endmacro( )





