## 目录及文件夹说明
|名称|说明|
|:--|:--|
|DemoWidget|对话框窗口，主程序|
|cmake|存放项目使用的cmake脚本|
|ProjectCommon|工程通用的一些源码|
|ext|存放第三方库|
|CMakeLists.txt|项目顶层cmake脚本文件|